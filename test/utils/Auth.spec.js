import Auth from '../../app/utils/Auth';

const auth = new Auth();

describe('Auth', () => {
  it('has the same properties that were saved', () => {
    const userAuth = auth.saveLoginData(
      'https://xnatdev.xnat.org',
      'admin',
      true
    );
    expect(userAuth.server).toBe('https://xnatdev.xnat.org');
    expect(userAuth.username).toBe('admin');
    expect(userAuth.allowInsecureSsl).toBe(true);
  });
});
