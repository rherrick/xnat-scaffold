// @flow
import React, { Component } from 'react';
import Modal from 'react-modal';
import { Link } from 'react-router-dom';
import UserAuth from '../utils/UserAuth';
import routes from '../constants/routes';
import Auth from '../utils/Auth';

Modal.setAppElement('body');

type Props = {
  server: UserAuth,
  auth: Auth,
  openModal: (
    modalTitle: string,
    modalMessage: string,
    onOk: () => void,
    onCancel: () => void
  ) => void
};

export default class XnatServerPanel extends Component<Props> {
  props: Props;

  constructor(props) {
    super(props);

    this.server = props.server;
    this.auth = props.auth;
    this.openModal = props.openModal;

    this.promptToDelete = this.promptToDelete.bind(this);
  }

  promptToDelete() {
    console.log(`About to delete server ${this.server.key}`);
    this.openModal(
      'Are you sure?',
      `You're about to delete the ${this.server.server} entry for user ${
        this.server.username
      }. Click 'OK' to continue or 'Cancel' to return.`,
      () => {
        console.log(`Deleting server entry ${this.server.key}`);
        this.auth.removeLoginData(this.server.key);
      }
    );
  }

  render() {
    const { server } = this.props;
    return (
      <span id={server.key} className="container-fluid">
        <Link to={routes.PLUGIN_TEST}>
          <button
            data-key={server.key}
            data-server={server.server}
            data-allow_insecure_ssl={server.allowInsecureSsl}
            className="server btn-bigfriendly btn-lg"
            type="button"
          >
            <div>
              <b>{server.alias}</b>
              <span className="user-name">{` User: ${server.username}`}</span>
            </div>
          </button>
        </Link>
        <Link to={`/manageServer/${this.server.key}`}>
          <button
            data-key={`edit-${server.key}`}
            className="btn-bigfriendly btn-lg"
            type="button"
          >
            <img src="../resources/icons/file-edit-16.png" alt="Edit" />
          </button>
        </Link>
        <button
          data-key={`delete-${server.key}`}
          className="btn-bigfriendly btn-lg"
          type="button"
          onClick={() => this.promptToDelete(server.key)}
        >
          <img src="../resources/icons/file-delete-16.png" alt="Delete" />
        </button>
      </span>
    );
  }
}
