// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import routes from '../constants/routes';
import Registry from '../plugins/registry';

type Props = {};

export default class XnatServers extends Component<Props> {
  props: Props;

  render() {
    const registry = new Registry();
    const pluginRoutes =
      registry.plugins.length > 0 ? (
        <ul>
          {registry.plugins.map(plugin => (
            <li>
              <Link to={`/${plugin.id}`}>{plugin.id}</Link>
            </li>
          ))}
        </ul>
      ) : (
        <ul>
          <li>No plugins configured</li>
        </ul>
      );
    return (
      <div className="container" data-tid="container">
        <h3>Plugin Test</h3>
        <div>{pluginRoutes}</div>
        <div>
          <Link to={routes.SERVERS}>Go back</Link>
        </div>
      </div>
    );
  }
}
