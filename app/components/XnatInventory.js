// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Modal from 'react-modal';
import routes from '../constants/routes';
import XnatServerList from './XnatServerList';
import Auth from '../utils/Auth';

type Props = {};

export default class XnatInventory extends Component<Props> {
  props: Props;

  constructor(props) {
    super(props);

    this.auth = new Auth();

    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.onOk = this.onOk.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.closeModal = this.closeModal.bind(this);

    this.state = {
      modalIsOpen: false,
      modalTitle: '',
      modalMessage: ''
    };
  }

  openModal(modalTitle, modalMessage, onOk: null, onCancel: null) {
    this.setState({
      modalIsOpen: true,
      modalTitle,
      modalMessage,
      onOk: onOk || this.closeModal,
      onCancel
    });
  }

  afterOpenModal() {
    const { modalTitle } = this.state;
    console.log(`Opened modal with title ${modalTitle}`);
  }

  onOk() {
    const { onOk } = this.state;
    if (onOk) {
      onOk();
    }
    this.closeModal();
  }

  onCancel() {
    const { onCancel } = this.state;
    if (onCancel) {
      onCancel();
    }
    this.closeModal();
  }

  closeModal() {
    this.setState({ modalIsOpen: false, modalTitle: '', modalMessage: '' });
  }

  render() {
    const { modalIsOpen, modalTitle, modalMessage } = this.state;
    return (
      <div className="container" data-tid="container">
        <div className="container-fluid">
          <Modal
            isOpen={modalIsOpen}
            onRequestClose={this.closeModal}
            contentLabel="authResult"
          >
            <h3>{modalTitle}</h3>
            <div>{modalMessage}</div>
            <button type="button" onClick={this.onOk}>
              OK
            </button>
            <button type="button" onClick={this.onCancel}>
              Cancel
            </button>
          </Modal>
          <div className="row">
            <div className="logo col-md-5">
              <img src="../resources/images/xnat_logo.jpg" alt="" />
              <div className="intro-text noselect">
                <p>
                  Welcome to XNAT Scaffold, where lonely medical apps can find a
                  friendly backend to consume and interact with.
                </p>
              </div>
            </div>
            <div className="col-md-7">
              <div className="row">
                <div className="col-12 text-left">
                  <Link to={routes.MANAGE_SERVER}>
                    <button
                      className="btn-bigfriendly button-row"
                      type="button"
                      data-toggle="modal"
                      data-target="#login"
                    >
                      <i className="fa fa-plus" />
                      <i className="fa fa-database" />
                      {' Add new XNAT server'}
                    </button>
                  </Link>
                  <XnatServerList
                    auth={this.auth}
                    openModal={this.openModal}
                    afterOpenModal={this.afterOpenModal}
                    closeModal={this.closeModal}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
