// @flow
import React, { Component } from 'react';
import Modal from 'react-modal';
import { Link, withRouter } from 'react-router-dom';
import routes from '../constants/routes';
import Auth from '../utils/Auth';
import Registry from '../plugins/registry';

type Props = {
  match: any
};

Modal.setAppElement('body');

class ManagePlugin extends Component<Props> {
  props: Props;

  constructor(props) {
    super(props);

    this.registry = new Registry();

    const { pluginId } = props.match.params;

    let state = {};
    if (pluginId && pluginId !== ':pluginId') {
      const plugin = this.registry.findPlugin(pluginId);
      this.pageTitle = `Edit plugin ${plugin.id}`;
      state = {
        id: plugin.id,
        version: plugin.version,
        location: plugin.location,
        enabled: plugin.enabled
      };
    } else {
      this.pageTitle = 'Add plugin';
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.onFieldChange = this.onFieldChange.bind(this);
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);

    this.state = {
      modalIsOpen: false,
      modalTitle: '',
      modalMessage: '',
      ...state
    };
  }

  handleSubmit(evt) {
    console.log(this.state);

    evt.preventDefault();

    const { id, version, location, enabled } = this.state;

    this.registry.createPlugin(
      id,
      version,
      location,
      enabled || true
    );
  }

  onFieldChange(property, evt) {
    const state = {};

    state[property] =
      evt.target.type === 'checkbox' ? evt.target.checked : evt.target.value;

    console.log(state);

    this.setState(state);
  }

  openModal(modalTitle, modalMessage) {
    this.setState({ modalIsOpen: true, modalTitle, modalMessage });
  }

  afterOpenModal() {
    const { modalTitle } = this.state;
    console.log(`Opened modal with title ${modalTitle}`);
  }

  closeModal() {
    this.setState({ modalIsOpen: false, modalTitle: '', modalMessage: '' });
  }

  render() {
    const { modalIsOpen, modalTitle, modalMessage } = this.state;
    return (
      <div id="managePlugin" className="container-fluid">
        <Modal
          isOpen={modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          contentLabel="authResult"
        >
          <h3>{modalTitle}</h3>
          <div>{modalMessage}</div>
          <button type="button" onClick={this.closeModal}>
            Close
          </button>
        </Modal>
        <div className="modal-content">
          <form action="" id="loginForm" onSubmit={this.handleSubmit}>
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">
                {this.pageTitle}
              </h5>
            </div>
            <div className="modal-body">
              <div className="form-group">
                <label htmlFor="pluginId" className="col-form-label">
                  Plugin ID:
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="pluginId"
                  id="pluginId"
                  placeholder="xnat.server.url"
                  defaultValue={this.state.server}
                  required
                  onChange={evt => {
                    this.onFieldChange('server', evt);
                  }}
                />
              </div>
              <div className="form-check text-right">
                <input
                  className="form-check-input"
                  type="checkbox"
                  defaultChecked={this.state.allowInsecureSsl}
                  name="allow_insecure_ssl"
                  id="allow_insecure_ssl"
                  onChange={evt => {
                    this.onFieldChange('allowInsecureSsl', evt);
                  }}
                />
                <label
                  className="form-check-label"
                  htmlFor="allow_insecure_ssl"
                >
                  Allow unverified certificates
                </label>
              </div>
              <div className="form-group">
                <label htmlFor="server" className="col-form-label">
                  Alias:
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="alias"
                  id="alias"
                  defaultValue={this.state.alias}
                  placeholder="(Optional) Give your XNAT an alias name."
                  required
                  onChange={evt => {
                    this.onFieldChange('alias', evt);
                  }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="username" className="col-form-label">
                  Username:
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="username"
                  id="username"
                  defaultValue={this.state.username}
                  onChange={evt => {
                    this.onFieldChange('username', evt);
                  }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="password" className="col-form-label">
                  Password:
                </label>
                <input
                  type="password"
                  className="form-control"
                  name="password"
                  id="password"
                  onChange={evt => {
                    this.onFieldChange('password', evt);
                  }}
                />
              </div>
            </div>
            <div className="modal-footer">
              <Link to={routes.SERVERS}>
                <button
                  type="button"
                  className="btn btn-gray"
                  data-dismiss="modal"
                >
                  Cancel
                </button>
              </Link>
              <button type="submit" className="btn btn-blue">
                Login
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default withRouter(ManagePlugin);
