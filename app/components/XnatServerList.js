// @flow
import React, { Component } from 'react';
import Auth from '../utils/Auth';
import XnatServerPanel from './XnatServerPanel';
import '../app.global.css';

type Props = {
  auth: Auth,
  openModal: (
    modalTitle: string,
    modalMessage: string,
    onOk: () => void,
    onCancel: () => void
  ) => void
};

export default class XnatServerList extends Component<Props> {
  props: Props;

  constructor(props) {
    super(props);

    this.auth = props.auth;
    this.state = {
      servers: this.auth.servers
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(server) {
    console.log(`You clicked the server ${server}`);
  }

  render() {
    console.warn(this.state);

    const { servers } = this.state;
    const { openModal } = this.props;
    const serverList = servers.map(server => (
      <XnatServerPanel
        key={server.key}
        server={server}
        auth={this.auth}
        openModal={openModal}
      />
    ));
    return <div className="">{serverList}</div>;
  }
}
