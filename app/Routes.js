import React from 'react';
import { Switch, Route } from 'react-router';
import routes from './constants/routes';
import App from './containers/App';
import XnatInventoryPage from './containers/XnatInventoryPage';
import ManageServerPage from './containers/ManageServerPage';
import PluginTestPage from './containers/PluginTestPage';
import LazyLoadModule from './plugins/lazy';
import Registry from './plugins/registry';
import ManagePluginPage from './containers/ManagePluginPage';

const registry = new Registry();

const pluginRoutes = registry.plugins.map(plugin => (
  <Route
    path={plugin.key}
    component={props => (
      <LazyLoadModule {...props} resolve={() => import(plugin.value)} />
    )}
  />
));

const RouteWithSubRoutes = route => (
  <Route
    path={route.path}
    render={props => (
      // pass the sub-routes down to keep nesting
      <route.component {...props} routes={route.routes} />
    )}
  />
);

export default () => (
  <App>
    <Switch>
      <Route path={routes.PLUGIN_TEST} component={PluginTestPage} />
      <Route path={routes.MANAGE_PLUGIN} component={ManagePluginPage} />
      <Route path={routes.MANAGE_SERVER} component={ManageServerPage} />
      <Route path={routes.SERVERS} component={XnatInventoryPage} />
      {pluginRoutes.map(route => (
        <RouteWithSubRoutes key={route.path} {...route} />
      ))}
    </Switch>
  </App>
);
