// @flow
import React, { Component } from 'react';
import ManageServer from '../components/ManageServer';

type Props = {
  serverKey: string
};

export default class ManageServerPage extends Component<Props> {
  props: Props;

  render() {
    const { serverKey } = this.props;
    return <ManageServer serverKey={serverKey} />;
  }
}
