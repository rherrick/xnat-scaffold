// @flow
import React, { Component } from 'react';
import PluginTest from '../components/PluginTest';

type Props = {};

export default class PluginTestPage extends Component<Props> {
  props: Props;

  render() {
    console.log(this.props);
    return <PluginTest />;
  }
}
