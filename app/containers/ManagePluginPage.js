// @flow
import React, { Component } from 'react';
import ManagePlugin from '../components/ManagePlugin';

type Props = {};

export default class ManagePluginPage extends Component<Props> {
  props: Props;

  render() {
    console.log(this.props);
    return <ManagePlugin />;
  }
}
