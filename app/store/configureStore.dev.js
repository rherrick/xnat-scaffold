import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';
import { createHashHistory } from 'history';
import { routerMiddleware, routerActions } from 'connected-react-router';
import { createLogger } from 'redux-logger';
import createRootReducer from '../reducers';

const history = createHashHistory();

const rootReducer = createRootReducer(history);

const configureStore = (initialState?: '') => {
  // Redux Configuration
  const middleware = [];
  const enhancers = [];

  // Thunk Middleware
  middleware.push(thunk);

  // Logging Middleware
  const logger = createLogger({
    level: 'info',
    collapsed: true
  });

  // Skip redux logs in console during the tests
  if (process.env.NODE_ENV !== 'test') {
    middleware.push(logger);
  }

  // Router Middleware
  const router = routerMiddleware(history);
  middleware.push(router);

  // Redux DevTools Configuration
  const actionCreators = {
    ...routerActions
  };
  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
  /* eslint-disable no-underscore-dangle */
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Options: http://extension.remotedev.io/docs/API/Arguments.html
        actionCreators
      })
    : compose;
  /* eslint-enable no-underscore-dangle */

  // Apply Middleware & Compose Enhancers
  enhancers.push(applyMiddleware(...middleware));
  const enhancer = composeEnhancers(...enhancers);

  // BEGIN: Added from dynamic code sample
  const injectAsyncReducers = (store, name, reducers) => {
    let asyncReducers;

    if (typeof reducers === 'function') {
      asyncReducers = reducers;
    }

    if (typeof reducers === 'object') {
      asyncReducers = combineReducers(reducers);
    }

    // eslint-disable-next-line no-param-reassign
    store.asyncReducers[name] = asyncReducers;
    store.replaceReducer(
      combineReducers({
        ...configureStore,
        ...store.asyncReducers
      })
    );
  };
  // END: Added from dynamic code sample

  // Create Store
  const store = createStore(rootReducer, initialState, enhancer);

  // BEGIN: Added from dynamic code sample
  store.asyncReducers = {};
  store.addDynamicModule = ({ name, reducers }) => {
    console.info(`Registering module reducers for ${name}`);
    injectAsyncReducers(store, name, reducers);
  };
  store.removeDynamicModule = name => {
    console.info(`Unregistering module reducers for ${name}`);
    const noopReducer = (state = {}) => state;
    injectAsyncReducers(store, name, noopReducer);
  };
  // END: Added from dynamic code sample

  if (module.hot) {
    module.hot.accept(
      '../reducers',
      // eslint-disable-next-line global-require
      () => store.replaceReducer(require('../reducers').default)
    );
  }

  return store;
};

export default { configureStore, history };
