import settings from 'electron-settings';
import Plugin from './plugin';

export default class Registry {
  constructor() {
    if (!settings.get('scaffoldPlugins')) {
      settings.set('scaffoldPlugins', []);
    }
    this.plugins = settings.get('scaffoldPlugins');
  }

  createPlugin(id, version, location, enabled) {
    const plugin = new Plugin(id, version, location, enabled);
    this.addPlugin(plugin);
    return plugin;
  }

  addPlugin(plugin) {
    const existing = this.plugins.find(element => element.id === plugin.id);
    if (existing === undefined) {
      this.plugins.push(plugin);
    } else {
      existing.version = plugin.version;
      existing.location = plugin.location;
      existing.enabled = plugin.enabled;
    }
    settings.set('scaffoldPlugins', this.plugins);
  }

  findPlugin(id) {
    return this.plugins.find(element => element.id === id);
  }

  removePlugin(id) {
    const index = this.plugins.findIndex(element => element.id === id);
    this.plugins.splice(index, 1);
    settings.set('scaffoldPlugins', this.plugins);
  }
}
