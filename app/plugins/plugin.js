export default class Plugin {
  id: string;

  version: string;

  location: string;

  enabled: boolean;

  constructor(id, version, location, enabled) {
    this.id = id;
    this.version = version;
    this.location = location;
    this.enabled = enabled;
  }
}
