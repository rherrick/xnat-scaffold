import axios from 'axios';
import { URL } from 'url';
import { Agent } from 'https';
import store from 'store2';
import sha1 from 'sha1';
import electron from 'electron';
import settings from 'electron-settings';
import UserAuth, { encodeServerKey } from './UserAuth';

const { ipcRenderer, remote } = electron;

export default class Auth {
  constructor() {
    this.servers = this.constructor.retrieveServers();
    this.lastUsed = this.constructor.retrieveLastUsed();
    this.instances = {};
  }

  static retrieveServers() {
    if (!settings.get('servers')) {
      settings.set('servers', []);
    }
    return settings.get('servers');
  }

  static retrieveLastUsed() {
    return settings.get('lastUsed');
  }

  static storeServers(servers) {
    if (servers) {
      settings.set('servers', servers);
    } else {
      Auth.deleteServers();
    }
  }

  static storeLastUsed(lastUsed) {
    settings.set('lastUsed', lastUsed);
  }

  static deleteServers() {
    settings.delete('servers');
  }

  static deleteLastUsed() {
    settings.delete('lastUsed');
  }

  saveUserAuth(userAuth) {
    const key = encodeServerKey(userAuth.server, userAuth.username);
    const found = this.findServer(key);
    if (found === undefined) {
      this.servers.push(userAuth);
      this.constructor.storeServers(this.servers);
    }
    return userAuth;
  }

  findServer(key) {
    return this.servers.find(element => element.key === key);
  }

  saveLoginData(server, alias, username, allowInsecureSsl = false) {
    return this.saveUserAuth(
      new UserAuth({
        server,
        alias,
        allowInsecureSsl,
        username
      })
    );
  }

  removeLoginData(key) {
    const index = this.servers.findIndex(element => element.key === key);
    this.servers.splice(index, 1);
    this.constructor.storeServers(this.servers);
  }

  removeCurrentUser() {
    const userAuth = this.getUserAuth();
    if (userAuth && this.lastUsed === userAuth.server) {
      this.removeLastUsed();
    }
    this.removeUserAuth(userAuth);
  }

  setLastUsed(key) {
    this.lastUsed = key;
    this.constructor.storeLastUsed(key);
  }

  removeLastUsed() {
    this.lastUsed = null;
    this.constructor.deleteLastUsed();
  }

  setUserAuth(userAuth) {
    this.setLastUsed(userAuth.key);
    const global = remote.getGlobal('userAuth');
    global.key = userAuth.key;
    global.server = userAuth.server;
    global.username = userAuth.username;
    global.allowInsecureSsl = userAuth.allowInsecureSsl;
    ipcRenderer.send('print_global');
  }

  removeUserAuth() {
    const global = remote.getGlobal('userAuth');

    if (this.lastUsed === global.key) {
      this.removeLastUsed();
    }

    global.key = null;
    global.server = null;
    global.username = null;
    global.allowInsecureSsl = null;
    ipcRenderer.send('print_global');
  }

  loginPromise(userAuth, password) {
    this.setLastUsed(userAuth.key);

    const agent = Auth.getAgent(userAuth);
    const instance = axios.create({
      httpsAgent: agent
    });
    this.instances[userAuth.key] = instance;
    return instance.get(`${userAuth.server}/data/auth`, {
      auth: {
        username: userAuth.username,
        password
      }
    });
  }

  static getAgent(userAuth) {
    const url = new URL(userAuth.server);
    const port = url.port || (url.protocol === 'https:' ? 443 : 80);
    return new Agent({
      host: url.host,
      port,
      path: url.pathname,
      rejectUnauthorized: !userAuth.allowInsecureSsl
    });
  }

  logoutPromise(userAuth) {
    const instance =
      this.instances[userAuth.key] ||
      axios.create({
        httpsAgent: new Agent({
          rejectUnauthorized: !userAuth.allowInsecureSsl
        })
      });
    return instance.get(`${userAuth.server}/app/action/LogoutUser`);
  }

  getUserAuth() {
    const global = remote.getGlobal('userAuth');
    const userAuth = new UserAuth();
    userAuth.server = global.server;
    userAuth.key = global.key;
    userAuth.username = global.username;
    userAuth.allowInsecureSsl = global.allowInsecureSsl;
    return userAuth;
  }

  getCsrfToken(userAuth, createdOffset = 30) {
    const now = () => parseInt(new Date() / 1000, 10);

    return new Promise(resolve => {
      if (!store.has('csrf_token_cache')) {
        store.set('csrf_token_cache', []);
      }

      let csrfTokenCache = store.get('csrf_token_cache');

      const tokenId = sha1(userAuth.server + userAuth.username);
      const tokenMatch = csrfTokenCache.filter(token => tokenId === token.id);

      if (tokenMatch.length && tokenMatch[0].created + createdOffset > now()) {
        resolve(tokenMatch[0].token);
      } else {
        // if there are cached tokens with same id => remove them
        if (tokenMatch.length) {
          csrfTokenCache = csrfTokenCache.filter(token => tokenId !== token.id);
        }

        axios
          .get(`${userAuth.server}/`, {
            auth: userAuth
          })
          .then(response => {
            const csrfTokenRequestData = response.data;
            let m;
            let csrfToken = false;
            const regex = /var csrfToken = '(.+?)';/g;

            // eslint-disable-next-line no-cond-assign,promise/always-return
            while ((m = regex.exec(csrfTokenRequestData)) !== null) {
              // This is necessary to avoid infinite loops with zero-width matches
              if (m.index === regex.lastIndex) {
                regex.lastIndex += 1;
              }

              // eslint-disable-next-line prefer-destructuring
              csrfToken = m[1];
            }

            csrfTokenCache.push({
              id: tokenId,
              created: now(),
              token: csrfToken
            });

            store.set('csrf_token_cache', csrfTokenCache);

            resolve(csrfToken);
          })
          .catch(() => {
            resolve(false);
          });
      }
    });
  }

  getJSessionCookie(xnatUrl = false) {
    return new Promise((resolve, reject) => {
      let server;

      if (xnatUrl) {
        server = xnatUrl;
      } else if (this.lastUsed && this.servers[this.lastUsed]) {
        server = this.servers[this.lastUsed];
      } else {
        reject(new Error('getJSessionCookie() error: no server URL provided'));
      }

      const slashUrl = `${server}/`;

      const jsession = {
        id: null,
        expiration: null
      };

      // Query cookies associated with a specific url.
      remote.session.defaultSession.cookies.get(
        { url: slashUrl },
        (error, cookies) => {
          if (cookies.length) {
            cookies.forEach(item => {
              switch (item.name) {
                case 'JSESSIONID':
                  jsession.id = item.value;
                  break;
                case 'SESSION_EXPIRATION_TIME':
                  jsession.expiration = item.value;
                  break;
                default:
                  break;
              }
            });

            if (jsession.id && jsession.expiration) {
              resolve(
                `JSESSIONID=${jsession.id}; SESSION_EXPIRATION_TIME=${
                  jsession.expiration
                };`
              );
            } else {
              reject(new Error(`${xnatUrl} [No JSESSIONID Cookie]`));
            }
          } else {
            reject(new Error(`${xnatUrl} [No Cookies]`));
          }
        }
      );
    });
  }

  anonymizeResponse(data, anon = '***REMOVED***') {
    let conf;

    if (data.config) {
      conf = data.config;
    }

    if (data.error && data.error.config) {
      conf = data.error.config;
    }

    if (conf) {
      if (conf.auth && conf.auth.password) {
        conf.auth.password = anon;
      }

      if (conf.headers && conf.headers.Authorization) {
        conf.headers.Authorization = anon;
      }
    }

    return data;
  }
}
