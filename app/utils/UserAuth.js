export default class UserAuth {
  address: string;

  alias: string;

  username: string;

  allowInsecureSsl: boolean;

  constructor(props) {
    this.server = props.server;
    this.alias = props.alias;
    this.allowInsecureSsl = props.allowInsecureSsl;
    this.username = props.username;
    this.key = encodeServerKey(this.server, this.username);
  }
}

export function encodeServerKey(server, username) {
  return `${server}:${username}`.replace(/[:/@!.?&#]+/g, '_');
}
